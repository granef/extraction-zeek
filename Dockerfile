#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2020-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


# Use ubuntu:20.04 as the latest LTS with all common functionality
FROM ubuntu:20.04

# Set the working directory
WORKDIR /usr/local/bin/granef

# Copy the current directory contents into the container at working directory
COPY . /usr/local/bin/granef

# Install Python and Zeek
RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes python3-minimal python3-pip dma wget gnupg build-essential cmake \
	&& pip3 install --trusted-host pypi.python.org --no-cache-dir -r requirements.txt \
	&& sh -c "echo 'deb http://download.opensuse.org/repositories/security:/zeek/xUbuntu_20.04/ /' > /etc/apt/sources.list.d/security:zeek.list" \
	&& wget -nv http://download.opensuse.org/repositories/security:/zeek/xUbuntu_20.04/Release.key -O Release.key \
	&& apt-key add - < Release.key \
	&& apt-get update \
	&& apt-get install --no-install-recommends -y zeek-6.0 \
	&& cp /usr/local/bin/granef/zeek-config/hash_sha256.zeek /opt/zeek/share/zeek/site/hash_sha256.zeek \
    && echo "\n# Add SHA256 hash for files \n@load frameworks/files/hash-all-files\n@load hash_sha256\n" >> /opt/zeek/share/zeek/site/local.zeek \
    && /opt/zeek/bin/zkg install --force zeek-community-id \
    && echo "\n# Add Community ID to conn.log \n@load zeek-community-id\nredef CommunityID::seed = 1;\n" >> /opt/zeek/share/zeek/site/local.zeek \
	&& sed -e "/@load policy\/frameworks\/notice\/extend-email\/hostnames/ s/^#*/#/" -i /opt/zeek/share/zeek/site/local.zeek \
	&& sed -e "/@load protocols\/ssh\/geo-data/ s/^#*/#/" -i /opt/zeek/share/zeek/site/local.zeek \
	&& apt-get purge --yes python3-pip dma wget gnupg build-essential cmake \
	&& apt-get autoremove --yes

# Run communication-handler.py with arguments when container launches (CMD if there are no arguments) 
ENTRYPOINT ["python3", "communication_handler.py"]
