#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2020  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2020-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Script:
1. Loads PCAP file from a local directory.
2. The file (if there is any) is then transformed using Zeek to logs.
3. The result is saved to a local directory called 'extraction'.

Usage:=
* Obtain data via local directory:
    $ communication_handler.py -im -l -i <input_dir_or_file_path> -o <output_dir_path>
"""

import sys
import os
import pwd
import grp
import shutil
import subprocess
import argparse
import logging, coloredlogs


def fix_permissions(path):
    # uid = pwd.getpwnam(user).pw_uid
    # gid = grp.getgrnam(user).gr_gid

    # os.chown(path, uid, gid)
    os.chmod(path, 0o0777)
    for subdir, _, files in os.walk(path):
        for file in files:
            file_path = os.path.join(subdir, file)
            # os.chown(file_path, uid, gid)
            os.chmod(file_path, 0o0664)


def output_dir_already_contains_output_file(output_dir_path, output_file_extension):
    """
        Checks if an output file ends with a given extension in the output directory.
        
        :param output_dir_path: directory that is searched
        :param output_file_extension: specifies what file(s) to look for based on their ending(s)
        :return: True if file that ends with given extension exists, False otherwise
    """
    for file in os.listdir(output_dir_path):
        if file.endswith(output_file_extension):
            return True
    return False


def get_input_files_in_dir(dir_path, file_endings):
    """
        Searches a given directory for files with specified ending(s).

        :param dir_path: directory that is searched
        :param file_endings: specifies what files to look for based on their ending(s)
        :return: list of input files with correct ending(s)
    """
    found_input_files = []
    for filename in os.listdir(dir_path):
        for file_ending in file_endings:
            if filename.endswith(file_ending):
                found_input_files.append(dir_path + '/' + filename)
    return found_input_files


def check_input_availability(dir_or_file_path):
    """
        Check file availability in input_dir directory.

        :param dir_or_file_path: path to input directory
        :return: local input file path
    """
    # check whether given dir_or_file_path is the actual input
    if os.path.isfile(dir_or_file_path) and dir_or_file_path.endswith('.pcap'):
        return dir_or_file_path

    # if the input file is not found, treat given path as a directory and look for a single PCAP file
    if os.path.isdir(dir_or_file_path):
        logger.debug('Looking for a PCAP file in directory "' + dir_or_file_path + '"')
        found_pcap_files = get_input_files_in_dir(dir_or_file_path, ['.pcap'])
        if len(found_pcap_files) == 1:
            logger.debug('Found input file "' + found_pcap_files[0] + '"')
            return found_pcap_files[0]
        logger.debug('Directory "' + dir_or_file_path + '" contains: ' + str(found_pcap_files))

    # either none or multiple PCAP files were found (and it is not clear which one is supposed to be used as input)
    logger.error('No (single) input PCAP found')
    return ''


def get_input(path):
    """
        Check if input is available in local storage and is in expected format (PCAP).

        :param path: path to where the input should be located (directory or file)
        :return: path to input file
    """
    input_file = check_input_availability(path)
    return input_file


def define_parsed_arguments():
    """
        Add arguments to ArgumentParser (argparse) module instance.

        :return: parsed arguments
    """
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input', help='Input data directory or file path', default='/data/', required=True,
                        type=str)
    parser.add_argument('-o', '--output', help='Output data directory path', default='granef-extraction/zeek/',
                        type=str)
    parser.add_argument('-m', '--mounted', help='Mounted data directory path', default='/data/', type=str)
    parser.add_argument('-f', '--force', help='Forces to overwrite any files that are already present in the '
                        'output data directory', action='store_true')
    parser.add_argument('-l', '--log', choices=['debug', 'info', 'warning', 'error', 'critical'], help='Log level',
                        default='INFO')

    return parser.parse_args()


if __name__ == '__main__':
    args = define_parsed_arguments()

    logger = logging.getLogger("Extraction Zeek")
    coloredlogs.install(level=getattr(logging, args.log.upper()), fmt='%(asctime)s %(name)s [%(levelname)s]: %(message)s')

    input = args.input
    output = args.mounted + '/' + args.output
    output_tmp = '/tmp/' + args.output

    if args.input.replace('/', '') != args.mounted.replace('/', ''):
        input = args.mounted + '/' + args.input
    logger.debug('Arguments set to: input = ' + input + ', output = ' + output)

    input_pcap = get_input(input)
    input_pcap_tmp = '/tmp/' + os.path.basename(input_pcap)

    if input_pcap:
        if not os.path.isdir(output):
            # if the output directory does not yet exist, it is created
            os.makedirs(output_tmp)
            os.makedirs(output)

        if output_dir_already_contains_output_file(output, '.log') and not args.force:
            # if the output directory does already exist, it is checked whether it does
            # not already contain the output files (then the user is warned)
            logger.warning('The output directory (' + output + ') seems to already contain the corresponding output '
                        'file(s). If you wish to run this Module anyway, run it with the -f/--force option to '
                        'overwrite the current content of the output directory.')
            exit()

        # Copy the input file to /tmp/ to speed up the processing
        shutil.copy(input_pcap, input_pcap_tmp)
        logger.debug('Input data successfully copied to the /tmp/ directory')

        try:
            zeek_command = '/opt/zeek/bin/zeek -Cr ' + input_pcap_tmp + ' local.zeek "' + \
                            'Site::local_nets += {192.168.0.0/16, 172.16.0.0/12, 10.0.0.0/8, [fe80::]/10}; ' + \
                            'redef LogAscii::use_json=T; ' + \
                            'redef LogAscii::json_timestamps = JSON::TS_ISO8601;"'
            subprocess.run(zeek_command, shell=True, check=True, cwd=output_tmp)
            # Copy the result to the output directory
            logger.debug('Data successfully generated in the /tmp/ directory')
            shutil.copytree(output_tmp, output, dirs_exist_ok=True)
            logger.debug('Log files successfully generated to "' + output + '" directory')
            fix_permissions(output)

        except subprocess.CalledProcessError as e:
            logger.error('(CalledProcessError) When running zeek:\nCommand =' + str(e.cmd) + ',\nReturncode =' +
                         str(e.returncode) + ',\nOutput =' + str(e.output).replace('\n', '\\n'))
        except Exception as e:
            logger.error('When running zeek: ' + str(e))

    else:
        logger.warning('No log files generated')
