
<img  src="https://is.muni.cz/www/milan.cermak/granef/granef-logo.svg"  height="60px">

[**Graph-Based Network Forensics**](https://gitlab.ics.muni.cz/granef/granef)**: Extraction**

---
  
The Extraction Module takes a network packet capture (PCAP file) as an input and extracts information from it using the [Zeek](https://zeek.org/) Network Security Monitor. One input PCAP file can produce multiple log files which contain information about the captured network activity in JSON format.
 

### Requirements

- Docker 
- Python3
- Python3 packages in [requirements.txt](requirements.txt)

The installation can be performed using the following command:

```bash
$ git clone https://gitlab.ics.muni.cz/granef/extraction-zeek.git
```

Use the following command to build the Docker container:

```bash
$ docker build --tag=granef/extraction --pull .
```

### Usage

The Docker container can be either run separately with command line arguments or as part of the Granef toolkit with arguments set in the [granef.yml](https://gitlab.ics.muni.cz/granef/granef/-/blob/master/granef.yml) configuration file. 

The following arguments can be set:

| Short argument | Long argument | Description | Default | Required |
|-|-|-|-|-|
|`-i`|`--input`|Input data directory or file path|`/data/`|T|
|`-o`|`--output`|Output data directory path|`granef-extraction/zeek/`|F|
|`-m`|`--mounted`|Mounted data directory path|`/data/`|F|
|`-f`|`--force`|Forces to overwrite files in output directory||F|
|`-l`|`--log`|Log level (possible values: `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`)|`INFO`|F|

If the `--input` argument contains a directory path, the directory has to contain a single ".pcap" file. 

Use the following command to start the extraction:

```bash
$ docker run --rm -v <LOCAL_DIR>:<MOUNTED> granef/extraction -m <MOUNTED> -i <INPUT_DIR_OR_FILE_PATH> -o <OUTPUT_DIR_PATH>
```

Or with the default `--mounted` path:

```bash
$ docker run --rm -v <LOCAL_DIR>:/data/ granef/extraction -i <INPUT_DIR_OR_FILE_PATH> -o <OUTPUT_DIR_PATH>
```

Please note that the `<INPUT_DIR_OR_FILE_PATH>` needs to be in the `<LOCAL_DIR>`.

The Extraction Module then performs the following steps:

1. Loads PCAP file from a local directory.
2. Generates log files using Zeek with the PCAP file as input.
3. Saves the generated log files to a local output directory.
